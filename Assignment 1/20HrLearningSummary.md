## **20 HOURS TO LEARN ANYTHING**
                                            
![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__20_.png)                   
 So, basically this is a very famous approach to learn a new skill.
 This approach shares a very **_efficient and doable_** approach at how to learn a new skill and *become decent at it* in a very small amount of time, even if you have a **Tight Schedule**.
 
 There is a common misconception amongst people that, to learn something, one needs around **10,000 Hours!!!**, which is truly a wrong fact. 
 _In fact, with just 20 Hrs of focussed and deliberate practice one can become decent at any new skill_ ;).    
 It is said that,  
 >***Whenever we start to learn a new skill, at first we are completely incompetent at that thing i.e. we know nothing about that. When we actually start learning it 
 >we learn very very fast in the start and get quite good at that thing in a very short time. Then the further peaks become difficult to achieve and takes a lot of time to improve.***
 
 ![This is the learning curve](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__22_.png)
 
 So basically, to learn anything and getting good at a considerable part of that thing will take only about ***20 HOURS!!***. 

#### In these 20 hours however one needs to be **_highly focused_** and should put **_deliberate, hardcore practice_**. Here are some 4 very easy steps following which _(properly)_ one can achieve this goal. These steps are :-

1. **De-Construct the skill :-**  This is the step in which we have to break the task in small parts and then try to learn the most important things first. In this way one can learn   
                                  more in a very short time.  
![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__45_.png)

2. **Learning Enough :-**         So, to start actually practicising a skill we don't need to learn or memorise everything about it or do phd on it but to do so we just need to
                                  learn enough that we can practice and get to know if we are wrong somewhere and then correct it. Then we can slowly - slowly add 
                                  to that knowlwdge and do more.

3. **Removing distractions :-**   So, we need to be very focussed in these 20 hours, for this we need to remove all the distractions such as mobile, laptops, internet, people,
                                  social media, e.t.c that stop us in doing so.

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__46_.png)    
4. **Practising for 20 hours :-** This right here is the most important part of this whole process. Usually whenever we start to learn a new skill, we are stupid at it and 
                                  we know it, but we do not want to feel stupid and hence we can't give our 100% to it and want to quit doing it. This acts as a barrier, so by 
                                  commiting to yourself before starting to learn a new skill, that you will give atleast 20 hours of dedicated practice to it, will help you and 
                                  push you to practice at least for 20 hours properly and will stop you from quitting.  

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__24_.png)

                                    
                                    
 ***So, this was a summary about the 20 Hours learning process.***