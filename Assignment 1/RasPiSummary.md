# **_Raspberry pi_**

* Tiny - credit card sized, low - cost device.
* Can be used for research projects such as weather monitoring and also for education purposes.
* The processor on it is Broadcom BCM3285 system - on - chip multimedia processor.
* It needs an operating system to work, i.e. Raspbian.

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__33_.png)

### **Some of the important things that we get on a raspberry pi board :**

* 1.2 GHz processor with 1GB integrated RAM :- _this is the processor used in pi_ .
* 2 HDMI port :- _For audio/video digital output_.
* 1 3.5mm jack.
* 4 USB ports (2 USB2.0 & 2 USB3.0) :- _for connecting input devices_.
* 1 micro sd card reader :- _Can be used for loading the operating system_.
* 1 Ethernet LAN port.
* 1 microUSB power port.
* 1 GPIO(General purpose input - output) interface.

### **What is a GPIO interface**  
> ##### The Raspberry Pi comes with a set of 40 exposed vertical pins on the board. These pins are a General Purpose Input/Output interface.

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__35_.png)

### GPIO pins of pi :

* #### Voltages :
  ###### Two 5V pins and two 3V3 pins are present on the board, as well as a number of ground pins (0V).
* #### Outputs :
  ##### A GPIO pin designated as an output pin can be set to high (3V3) or low (0V).
* #### Inputs :
  ##### A GPIO pin defined as an input pin can be set to high (3V3) or low (0V).

### Some advantages and disadvantages of raspberry pi are shown below :
![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__42_.png)

> ## _So, this was the summary about Raspberry pi_.