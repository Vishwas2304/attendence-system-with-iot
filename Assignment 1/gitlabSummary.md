# **Basic Gitlab Workflow**

> #### **Git** is a **_version control software_**. It lets you easily keep track of every revision you and your team make during the development of your software. You all share _one repository_ of code that is worked on independently and then _merged_ back together.

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__48_.png)

## **Gitlab Terminology...**  
> #### **There are some words which are new to every new gitlab user, these are :-**

**1. Repository :-** This is the place where all your work _(projects, activities)_ are present. It's a collection of files and folders of your projects.

**2. Fork and clone :-** Fork is done to import any given project of a user into your repository and work on it independently. Whereas, cloning is just making a copy of the
                         project into your local computer. When you clone a project you get a link that can be copied.
                         
**3. Commit :-** Commiting means saving your work in your repository. If you don't commit any unsaved work it will get lost. It is a very important step.

**4. Merge and pull request :-** Merging refers to merging _(adding)_ your work to the master branch so that it could be a part of the main project. Pull request refers to sending
                                 a request to the master user to check your work. If your work is correct the master will merge it else he will give you a feedback on
                                 where your project was wrong and what needs to be done to improve it.

> #### So, these were some of the terms useful in gitlab.  
##### When you see a project and want to contribute in it, you may fork it and work on it separately and then send a merge request to the main user and he may add your work.  

 **_Also you can create your own project, this can be done as follows :-_**  
### To create a project in GitLab:
![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__50_.png)

* ##### In your dashboard, click the green New project button or use the plus icon in the navigation bar. This opens the New project page.
* ##### In the new project page, select Blank document.


### To fork and clone:

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__26_.png)

##### On the project page, click on fork and then clone it.

### To add a file:

![](https://gitlab.com/Vishwas2304/attendence-system-with-iot/-/raw/master/Assignment%201/Images/Screenshot__29_.png)

##### On the project page, click on the + icon, then click on upload file to upload any file.

> #### After doing all these things when you feel like your work is complete, send a merge request to the master using the "Send merge request" button on the project page and **_hope for the best :)_**

 

 
