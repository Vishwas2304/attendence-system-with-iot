# Attendence system with IoT

# Skilling Orientation Module

*This is the first step in your journey with us. This module will make you familiar with our workflow*
## What you will you learn in Skilling Orientation module?
1. Culture and the Mission of project.

2. Faster approach towards learning - 20hrs learning method

3. Basic Git Work-flow

4. Project management - GitLab Interface

5. Document like a professional coder

6. Coding Style

7. Basics of Raspberry pi

8. Usable software for this project.

9. Advance Image processing to recognize face .

> How to complete each Module including orientation ? (The Skilling Process)
Learning a skill  is divided into  3 stages(USP) :-
- Stage 1- Understand (U)

- Stage 2 -Summarize (S)

- Stage 3- Practice (P)


| Process Stage | Description |
| ------ | ------ |
| Understand | Read the course material and try to understand topics covered in the Module. |
| Summarize (Assignment)| Reinforce critical parts of the subject by writing them down in concise  Summaries.|
|Practice  (Assignment)|Practice skills, to complete the learning process. This is where your brain learns the most.|
|Submit Assignments|Submitted assignments are graded and get feedback from the mentors|


# Navigating  This Skilling Course in Gitlab
1. Understand Stage: Find the Reading Material for every Module in the Module Wiki.
2. Summarize Assignments: Find the Summarize Assignments in the Module Issues.
3. Practice Assignments: Find the Practice Assignments in the Module Issues.
4. Submitting Assignments: Find the Issue to submit Assignments in the Module Issues.

![image](https://gitlab.iotiot.in/newbies/orientation/raw/master/extras/01.png)



# Asking Questions/Queries about the Module
![image](https://gitlab.iotiot.in/newbies/orientation/raw/master/extras/03.png)
